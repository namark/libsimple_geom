#ifndef SIMPLE_GEOM_BOX_HPP
#define SIMPLE_GEOM_BOX_HPP

#include "vector.hpp"
#include "simple/support/range.hpp"

namespace simple::geom
{

	template <typename Type>
	struct segment
	{
		Type size, position;

		using range = support::range<Type>;

		[[nodiscard]]
		constexpr operator range() const
		{
			return range{position, position + size};
		}

		// NOTE: hmmmm, what about the dimensions thouuuuugh...
		friend bool operator==(const segment& one, const segment& other)
		{ return one.size == other.size && one.position == other.position; }
		friend bool operator!=(const segment& one, const segment& other)
		{ return !(one == other); }

	};

	template <typename T> segment(T, T) -> segment<T>;

	template <typename Type, typename AnchorType = Type>
	struct anchored_segment : public segment<Type>
	{
		using base = segment<Type>;
		AnchorType anchor;

		constexpr anchored_segment() = default;

		[[nodiscard]]
		constexpr operator typename base::range() const
		{
			auto lower = this->position -
				static_cast<Type>(anchor * static_cast<AnchorType>(this->size));
			return typename base::range{lower, lower + this->size};
		}

		friend bool operator==(const anchored_segment& one, const anchored_segment& other)
		{ return segment<Type>::operator==(one, other) && one.anchor == other.anchor; }
		friend bool operator!=(const anchored_segment& one, const anchored_segment& other)
		{ return !(one == other); }

	};

	template <typename Coordinate, size_t Dimensions = 2>
	using vector_segment = segment<vector<Coordinate, Dimensions>>;

	template <typename Coordinate, size_t Dimensions = 2, typename Anchor = Coordinate>
	using anchored_vector_segment = anchored_segment<vector<Coordinate, Dimensions>, vector<Anchor, Dimensions>>;

} // namespace simple::geom

#endif /* end of include guard */
